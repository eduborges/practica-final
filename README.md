Rebirth the town
=================

Este proyecto es la Práctica final de la asignatura **Programación Unity 2D** del **Master en diseño y desarrollo de videojuegos** de la **Universitat Oberta de Catalunya**.

Es un juego basado en Stardew Valley, pero con mucho menos contenido.

Implementación
------------------

**Movimiento del personaje**:

- El jugador puede moverse en 4 direcciones desde un mapa top n down del estilo Pokemon.
- Para ser eficientes en la gestion de animaciones se han usado blend trees, que representen en la misma animación pero en sentido diferente
- El personaje tiene un ridigbody sin que le afecte la gravedad
- Se mueve entre varias capas de tilemaps, con distintos sorting layers y algunos implementando closiones y otros no. Todo esto para generar más realismo en el mapa. Por ejemplo, que al pasar por detrás de una casa, el tejado tape al personaje pero que no choque con él.

**Entidades**:

- Las entidades son objetos con los que el jugador puede abrir un cuadro de diálogo. Estos puedes ser NPCs, puertas de casas o puntos de información (carteles)
- Se comunican directamente con el cuadro de diálogo para indicar que tipo de mensaje se debe de mostrar, y le será devuelto cuando se conozca una respuesta, que ha seleccionado el usuario para así la entidad tomar la acción pertinente. (Si el jugador usa tocar timbre, la acción será abrir puerta, si el jugador decide irse, no habrá acción)
- Se les pueden definir horarios para que tengan diálogos distintos en horas del día distintas
- La información de los diálogos se guarda en formato JSON y está relacionada con cada una por un entityName. Si un NPC tiene como nombre de entidad Dimitrov, se irá a buscar su diálogo a Dimitrov.json cuando el jugador se acerque.

**Ataques del personaje**:

- El personaje tiene ancladas 4 partículas iguales pero orientadas en distinta dirección que representan el paso de su espada, como si de una estela de avión se tratase.
- Las párticulas de ataque tienen collision que son las que inflingen daño a los enemigos. Se emitirá una ráfaga de 30 cuando el jugador ataque en la dirección a la que está mirando.

**Enemigos**:

- Los enemigos siguen en línea recta al jugador cuando entra en el campo de visión y si le tocan le quitarán el recurso más preciado.
- Si chocan contra cualquier cosa se autodestruyen.
- Sueltan botín solo si mueren a manos del jugador.

**Sistema de siembra**:

- Una manera de conseguir dinero además de eliminar enemigos y comerciar con NPCs, es sembrar en la parte baja del mapa, donde hay tierra fertil. Con botón derecho se modificará el tilemap y donde antes solo había tierra, aparecerán unas hojitas que eclosionarán al cabo de un rato aleatorio.
- Permite conseguir vegetales y posibilidad de recuperar alguna semilla

**Recolectables**:

- Todos los items que el usuario puede coger del suelo, ya sean botines de enemigos o recogida de la siembra, pueden ser recogidos pasando el cursor por encima o tocándolos con el personaje.
- Son representados con el sprite del recurso que aportan.

**Popups**:

- Efecto emergente que surge al perder, ganar o no tener suficientes recursos para realizar una acción. Ayuda al gamefeel del usuario.
- Para que no se solapen al ocurrir cambios de recursos al mismo tiempo el PopupAgent gestiona una cola, avisado por los PopUpController.

**Monedero**:

- Es lo que se muestra en la zona y lo que define los recursos que posee el jugador y como estos se modifican

**TimeAgent**:

- Agente que gestiona el tiempo en el juego. Muestra la hora del día, cambia el color del ambiente dependiendo si es amanecer, mediodía, tarde o noche.
- Comprueba horarios, cobra impuestos diarios y gestiona el win condition.

**Teleporters**:

- Recurso utilizado para mover al jugador rápidamente de un lugar a otro. (Al entrar a una casa, al cambiar de la ciudad a la finca, etc)

**Dos pantallas de juego**:

- La primera escena de juego es un tutorial, que tratá de pequeñas explicaciones de texto animado con momentos de interacción para el aprendizaje por parte del usuario.
- La segunda es el juego en sí donde se puede desarrollar todo lo anterior.

**Pantalla de opciones**:

- Gestiona el nivel del volumen de la música
- Gestiona si es pantalla completa o no
- Gestiona la resolución a la que se mostrará la ventana de juego.


How to play
------------

- Don't be under 1000$ dollars at the end of the day or you lose.
- Try to survive with enough money 31 days to win.
- If enemies hit you, you lose money.
- You can trade resources with the towny NPCs.
- You can improve you weapon to get enemy loot easier.
- You can plant seeds at your terrains to get vegetables that are interchangeable.

Controls
--------

**Move Y axis** - ↑ ↓

**Move X axis** - ← →

**Attack** - _Left Click_

**Plant** - _Right Click on fertil terrain_

**Interact with entities** - _Space_

Play [here](https://github.com/alu0100885613/Rebirth-the-town/archive/master.zip)

Gameplay
--------

[![U2D](https://i.ytimg.com/vi/0fHUzJtZYaY/1.jpg)](https://www.youtube.com/watch?v=0fHUzJtZYaY "Rebirth the town")

_Click image to access Gameplay video_

Support
-------

mail: <eduborges@uoc.edu>

Author
------

[Eduardo Borges Fernández](https://gitlab.com/eduborges)

Credits
-------

**Dollar Sprite**:

- [Bombada](https://opengameart.org/content/bombada) by [richtaur](https://opengameart.org/users/richtaur) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

**Enemies**:

- [Micro Character Bases pack](https://thkaspar.itch.io/micro-character-bases) by [Kacper Woźniak](https://thkaspar.itch.io/) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

License
-------

The project is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).