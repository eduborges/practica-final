﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

/* Clase que controla las entidades de juego */

public class EntityController : MonoBehaviour {

    /// <summary>
    /// Información de cada diálogo
    /// </summary>
    [System.Serializable]
    public class DialogGraphData {
        public DisplayController.DisplayInfoType type;
        public bool isLast;
        public string[] content;
        public string[] actions;
    }
    
    /// <summary>
    /// Estado del grafo que contiene la información y sus conexiones
    /// </summary>
    public class DialogGraphState {
        public DialogGraphData data;
        public DialogGraphState[] next;
    }

    /// <summary>
    /// Tipos de entidad
    /// </summary>
    public enum EntityType {
        NPC,
        House,
        Poster
    }

    /// <summary>
    /// Estado actual del diálogo
    /// </summary>
    DialogGraphState currentState;

    /// <summary>
    /// Horario de apertura de la entidad
    /// </summary>
    public TimeAgent.Daytime[] schedule;

    /// <summary>
    /// Tipo de entidad
    /// </summary>
    public EntityType entityType;

    /// <summary>
    /// Nombre de la entidad mostrado al usuario
    /// </summary>
    public string externalName;

    /// <summary>
    /// Nombre de la entidad para buscarlo en los JSON
    /// </summary>
    public string entityName;

    /// <summary>
    /// Grid que contiene los tilemaps para conocer partes de la entidad hecha con tiles
    /// </summary>
    GridLayout gridLayout;

    /// <summary>
    /// Agente que gestiona la modificación de tiles en tiempo de ejecución
    /// </summary>
    TilemapAgent tilemapEraser;

    /// <summary>
    /// Panel de diálogo con el que comunicarse
    /// </summary>
    public GameObject dialogPanel;

    /// <summary>
    /// Condición que controla si hay alguien delante de la entidad
    /// </summary>
    bool someOneInTheDoor = false;

    /// <summary>
    /// Localización en el tilemap de la puerta en el caso de las entidades casa
    /// </summary>
    Vector3Int doorCell;
    

	// Use this for initialization
	void Start () {
        gridLayout = GameObject.Find("Grid").GetComponent<GridLayout>();
        tilemapEraser = GameObject.Find("TilemapEraser").GetComponent<TilemapAgent>();
	}
	
	// Update is called once per frame
    // Si hay alguien en la puerta y se pulsa espacio, inicia el diálogo
	void Update () {
        if (someOneInTheDoor)
            if (Input.GetKeyDown(KeyCode.Space))
                KnockKnockDoor();

	}

    /// <summary>
    /// Cuando se triggerea una entidad por el acercamiento de un jugador, se comprueba el horario si la entidad tiene y se cargan los diálogos relacionado con esta
    /// </summary>
    /// <param name="col">COllider contra el que se colisiona</param>
    void OnTriggerEnter2D(Collider2D col) {
        if(col.transform.tag == "Player") {
            if(schedule.Length == 2) {
                CheckSchedule();
            } else {
                currentState = DialogFiller.Fill(entityName);
            }

            someOneInTheDoor = true;
            doorCell = gridLayout.WorldToCell(col.transform.position);
        }
    }

    /// <summary>
    /// Si el usuario se aleja de la entidad el diálogo termina y se deja de escuhar la tecla de interacción (espacio)
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerExit2D(Collider2D col) {
        if (col.transform.tag == "Player"){
            someOneInTheDoor = false;
            dialogPanel.SetActive(false);
        }
    }

    /// <summary>
    /// Activa el diálogo con la entidad y le comunica al DisplayController el tipo de mensaje y el contenido a mostrar
    /// </summary>
    void KnockKnockDoor(){
        someOneInTheDoor = false;
        Dialog(true);

        if(currentState.data.type == DisplayController.DisplayInfoType.RawText) {
            dialogPanel.GetComponent<DisplayController>().DisplayRawText(externalName,currentState.data.content[0],this);
        } else {
            dialogPanel.GetComponent<DisplayController>().DisplayInteractive(externalName, currentState.data.content, this);
        }
 
    }

    /// <summary>
    /// Comprueba que la casa está abierta o cerrada para cambiar el tipo de diálogo
    /// </summary>
    void CheckSchedule() {
        if (entityType == EntityType.House) {
            if (!GameObject.Find("TimeAgent").GetComponent<TimeAgent>().BeetwenDaytimes(schedule[0], schedule[1]))
                    entityName = "houseClosed";
            else
                    entityName = "houseOpen";
        }
        currentState = DialogFiller.Fill(entityName);
    }

    /// <summary>
    /// Activa/Desactiva el diálogo y el movimiento del jugador al contrario del diálogo
    /// </summary>
    /// <param name="enabled">Activado?</param>
    void Dialog(bool enabled) {
        dialogPanel.SetActive(enabled);
        PlayerMovement(!enabled);
    }

    /// <summary>
    /// Desactiva el movimiento del jugador
    /// </summary>
    /// <param name="enabled">Activado?</param>
    public void PlayerMovement(bool enabled){
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().enabled = enabled;
    }

    /// <summary>
    /// Teleporta al jugador a la posición indicada
    /// </summary>
    /// <param name="position">Posición de destino</param>
    public void PlayerTeleport(Vector3 position) {
        GameObject.FindGameObjectWithTag("Player").transform.position = position;
    }

    /// <summary>
    /// Función que recoge la información del usuario al pasar de diálogo y comprueba si hay que realizar alguna acción y pasa al siguiente diálogo si existe
    /// </summary>
    /// <param name="index">Opción escogida</param>
    public void resultReady(int index) {
        if(currentState.data.actions.Length > index) {
            ActionsAssign(currentState.data.actions[index]);
        }

        if (!currentState.data.isLast) {
            currentState = currentState.next[index];
            KnockKnockDoor();
        } else {
            Dialog(false);
            someOneInTheDoor = true;
            currentState = DialogFiller.Fill(entityName);
        }
    }

    /// <summary>
    /// Acciones que realiza la entidad sobre el monedero del jugador
    /// TODO: Buscar otra solución a los datos hardcodeados
    /// </summary>
    /// <param name="action">nombre de la acción</param>
    void ActionsAssign(string action) {
        switch (action) {
            case "openDoor": { OpenDoor(); };break;
            case "closeDoor": { CloseDoor(); };break;
            case "comprarVrd": { Buy(PocketManager.Resources.Vegetable, 5, 10, PocketManager.Resources.Dollar); };break;
            case "comprarSml": { Buy(PocketManager.Resources.Seed, 5, 5, PocketManager.Resources.Dollar); }; break;
            case "venderVrd": { Buy(PocketManager.Resources.Dollar, 10, 10, PocketManager.Resources.Vegetable); }; break;
            case "venderPrt": { Buy(PocketManager.Resources.Dollar, 40, 10, PocketManager.Resources.Pirita); }; break;
            case "venderIcr": { Buy(PocketManager.Resources.Dollar, 20, 10, PocketManager.Resources.Icor); }; break;
            case "dados": {
                    if (RngWin(2, 12, 10)){
                        Buy(PocketManager.Resources.Dollar, 10, 5, PocketManager.Resources.Dollar);
                    } else {
                        Buy(PocketManager.Resources.Dollar, 0, 5, PocketManager.Resources.Dollar);
                    }
            };break;
            case "enterHouse": {
                    PlayerTeleport(transform.GetChild(0).transform.position);
                    CloseDoor();
            };break;

            case "elixirIcr":{ BuyFlask(PocketManager.Resources.Icor, 35); };break;
            case "elixirDlr":{ BuyFlask(PocketManager.Resources.Dollar, 200); }; break;
            case "elixirVrd":{ BuyFlask(PocketManager.Resources.Vegetable, 100); }; break;
            case "mejorarSwrd": { Buy(PocketManager.Resources.SwordPower, 4, 10, PocketManager.Resources.Pirita); };break;
        }
    }

    /// <summary>
    /// Abre la puerta
    /// </summary>
    void OpenDoor() {
        tilemapEraser.openDoor(doorCell);
    }

    /// <summary>
    /// Cierra la puerta
    /// </summary>
    void CloseDoor() {
        tilemapEraser.closeDoor(doorCell);
    }

    /// <summary>
    /// Cambia una cantidad de recursos por otra cantidad de otro
    /// </summary>
    /// <param name="buyResource">recurso a recibir</param>
    /// <param name="amount">cantidad de recurso a recibir</param>
    /// <param name="price">cantidad de recurso a dar</param>
    /// <param name="payResource">recurso a dar</param>
    void Buy(PocketManager.Resources buyResource, int amount, int price, PocketManager.Resources payResource) {
        PocketManager Pocket = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();
        
        if (Pocket.Amount(payResource) < price) {
            PopupAgent Popup = GameObject.FindGameObjectWithTag("Popup").GetComponent<PopupAgent>();
            Popup.NotEnoughResources(payResource);
        } else {
            Pocket.AddResources(buyResource, amount);
            Pocket.AddResources(payResource, -price);
        }
    }

    /// <summary>
    /// Saca un resultado aleatorio entre un máximo un mínimo y un porcentaje
    /// </summary>
    /// <param name="min">mínimo posible a obtener</param>
    /// <param name="max">máximo posible a obtener</param>
    /// <param name="minToWin">minimo obtenido para ganar</param>
    /// <returns></returns>
    bool RngWin(int min, int max, int minToWin) {
        int coin = Random.Range(min, max + 1);

        if(coin >= minToWin){
            return true;
        } else {
            return false;
        }
    }

    /// <summary>
    /// Le da al usuario una mejora en la espada y modifica el color de la UI de está
    /// </summary>
    /// <param name="resource">recurso a dar a cambio</param>
    /// <param name="amount">cantidad de recurso a dar a cambio</param>
    void BuyFlask(PocketManager.Resources resource, int amount) {
        Buy(PocketManager.Resources.SwordPower, 200, amount, resource);
        GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>().improvedSword = true;
        Invoke("FlaskEnds", 90f);
    }

    /// <summary>
    /// Termina la mejora de la espaday vuelve a su estado normal
    /// </summary>
    void FlaskEnds() {
        Buy(PocketManager.Resources.SwordPower, 0, 200, PocketManager.Resources.SwordPower);
        GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>().improvedSword = false;
    }
}
