﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla la colisión de las partículas de la espada */

public class SlashSwordCollisionController : MonoBehaviour {

    /// <summary>
    /// Monedero del jugador para conocer el poder de la espada
    /// </summary>
    PocketManager pocket;

    /// <summary>
    /// Ha existido ya una colisión previa con estas partículas?
    /// </summary>
    public bool firstCollision = false;

    /// <summary>
    /// La primera colisión con las partículas dañará al enemigo que haya chocado contra ellas
    /// </summary>
    /// <param name="other">GameObject que choca contra las partículas</param>
    void OnParticleCollision(GameObject other) {
        if (!firstCollision) {
            if(!pocket)
                pocket = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();
            firstCollision = true;
            other.GetComponent<EnemyController>().GetDamaged(pocket.swordPower);
        }
    }
}
