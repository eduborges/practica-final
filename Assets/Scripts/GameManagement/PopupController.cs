﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla cada popup */

public class PopupController : MonoBehaviour {

    /// <summary>
    /// Agente de los popup que los gestiona
    /// </summary>
    PopupAgent myAgent;

	// Use this for initialization
	void Start () {
        myAgent = GameObject.FindGameObjectWithTag("Popup").GetComponent<PopupAgent>();
	}
	
    /// <summary>
    /// Avisa al agente que es turno del siguiente popup porque esté ya ha terminado
    /// </summary>
	public void NextPopup() {
        myAgent.NextInQueue();
    }

    /// <summary>
    /// Se autodestruye el objeto popup
    /// </summary>
    public void AutoDestroy(){
        Destroy(gameObject);
    }
}
