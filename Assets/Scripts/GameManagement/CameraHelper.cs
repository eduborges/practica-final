﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/* Clase que gestiona la activación de elementos al entrar en cámara*/
public class CameraHelper : MonoBehaviour {

    /// <summary>
    /// Distancia del mundo entre el centro de la pantalla y los bordes
    /// </summary>
    Vector2 screenDistance;

    /// <summary>
    /// Posición actual de la cámara principal
    /// </summary>
    Vector3 cameraPos;

    /// <summary>
    /// Collider de la cámara que activará el movimiento de los enemigos al tocarlos
    /// </summary>
    BoxCollider2D freezeCollider;

    // Use this for initialization
    void Start () {
        calculateCameraData();
        freezeCollider = gameObject.AddComponent<BoxCollider2D>();
        freezeCollider.isTrigger = true;
        attachCollider();
    }
	
	// Update is called once per frame
	void Update () {
        calculateCameraData();
        attachCollider();
    }

    /// <summary>
    /// Calcula el tamaño de las medidas de la cámara
    /// </summary>
    void calculateCameraData(){
        cameraPos = Camera.main.transform.position;
        screenDistance.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)));
        screenDistance.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)));
    }

    /// <summary>
    /// Coloca un collider en la misma posición de la cámara y con su mismo tamaño
    /// </summary>
    void attachCollider() {
        freezeCollider.transform.position = cameraPos;
        freezeCollider.size = new Vector2(screenDistance.x, screenDistance.y);
    }
}