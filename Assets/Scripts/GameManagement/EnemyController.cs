﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla a cada enemigo */

public class EnemyController : MonoBehaviour {

    /// <summary>
    /// Botín variable que puede soltar un enemigo
    /// </summary>
    [System.Serializable]
    public struct EnemyLoot {
        public PocketManager.Resources resources;
        public int minAmount;
        public int maxAmount;
    }

    /// <summary>
    /// Efecto de destrucción del enemigo
    /// </summary>
    public ParticleSystem destroyParticles;

    /// <summary>
    /// Condición que evita que el enemigo se mueva hasta que entre en rango de visión
    /// </summary>
    bool freeze = true;

    /// <summary>
    /// Objeto jugador que conoce cada enemigo para poder seguirlo
    /// </summary>
    GameObject player;

    /// <summary>
    /// Velocidad a la que se mueve el enemigo
    /// </summary>
    public float speed = 10;

    /// <summary>
    /// Sprite renderer del enemigo, es un atributo para poder acceder a un menor coste
    /// </summary>
    SpriteRenderer spriteRenderer;

    /// <summary>
    /// Cantidad de dinero que quita el enemigo si choca contra el jugador
    /// </summary>
    public int AmountOfMoneyDestructionOnHit = -50;

    /// <summary>
    /// Vida del enemigo
    /// </summary>
    public int healthPoints = 350;

    /// <summary>
    /// Prefab de los recolectables a spawnear con el botín del enemigo
    /// </summary>
    public GameObject collectibleItem;

    /// <summary>
    /// Botines que suelta al morir por el jugador
    /// </summary>
    public EnemyLoot[] loots;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        spriteRenderer = GetComponent<SpriteRenderer>();

    }
	
	// Update is called once per frame
	void Update () {
        if(!freeze)
            RunIntoPlayer();

        //Siempre mira hacia donde está el jugador
        if(player.transform.position.x < transform.position.x) {
            spriteRenderer.flipX = true;
        } else {
            spriteRenderer.flipX = false;
        }
    }

    /// <summary>
    /// El enemigo se moverá en línea recta hacia la posición del jugador
    /// </summary>
    void RunIntoPlayer() {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }

    /// <summary>
    /// Si un enemigo choca contra el jugador, le quita la cantidad definida del monedero, si no tiene tanta, le quita lo que le quede. Si choca contra un prop, simplemente muere con su efecto
    /// </summary>
    /// <param name="col">Collider contra el que se choca</param>
    void OnCollisionEnter2D(Collision2D col) {
        if(col.transform.tag == "Player") {
            PocketManager pocket = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();
            if(pocket.Amount(PocketManager.Resources.Dollar) < Mathf.Abs(AmountOfMoneyDestructionOnHit)){
                pocket.AddResources(PocketManager.Resources.Dollar, -pocket.Amount(PocketManager.Resources.Dollar));
            } else {
                pocket.AddResources(PocketManager.Resources.Dollar, AmountOfMoneyDestructionOnHit);
            }
            
        }
        DestroyEffect();
        Destroy(gameObject);
    }

    /// <summary>
    /// Si la cámara triggerea al enemigo esté se empieza a mover
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter2D(Collider2D col) {
        if(col.transform.tag == "MainCamera" && freeze){
            freeze = false;
        }
    }

    /// <summary>
    /// Daña al enemigo y lo destruye y spawnea el loot si le provoca la muerte
    /// </summary>
    /// <param name="damageAmount">Daño que recibe el enemigo</param>
    public void GetDamaged(int damageAmount){
        healthPoints = healthPoints - damageAmount;
        AudioAgent.enemyDamagedAudio.GetComponent<AudioSource>().Play();
        if(healthPoints <= 0) {
            DestroyEffect();
            dropLoot();
            Destroy(gameObject);
        } else {
            StartCoroutine(DamagedEffect());
        }
    }


    /// <summary>
    /// Efecto parpadeante del enemigo al recibir daño
    /// </summary>
    /// <returns>IEnumerator</returns>
    public IEnumerator DamagedEffect() {
        freeze = true;
        for(int i = 0; i < 10; i++){
            spriteRenderer.enabled = !spriteRenderer.enabled;
            yield return new WaitForSeconds(0.1f);
        }
        freeze = false;
        spriteRenderer.enabled = true;
    }

    /// <summary>
    /// Spawnea el loot del enemigo
    /// </summary>
    public void dropLoot(){
        PocketManager pocket = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();
        foreach (EnemyLoot loot in loots) {
            GameObject go = Instantiate(collectibleItem);
            go.transform.position = transform.position;
            go.transform.localScale = new Vector3(1f, 1f, 1f);
            go.GetComponent<SpriteRenderer>().sprite = pocket.GetResourceImage(loot.resources);
            go.GetComponent<CollectibleItem>().resource = loot.resources;
            go.GetComponent<CollectibleItem>().amount = Random.Range(loot.minAmount, loot.maxAmount + 1);
        }
    }

    /// <summary>
    /// Se instancia el efecto de destrucción en la posicón del enemigo
    /// </summary>
    void DestroyEffect() {
        ParticleSystem ps = Instantiate(destroyParticles);
        ps.transform.position = transform.position;
        ps.Emit(30);
    }

}
