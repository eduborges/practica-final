﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controlla las acciones del jugador */

public class PlayerController : MonoBehaviour {

    /// <summary>
    /// Direcciones a las que se puede mirar
    /// </summary>
    enum Direction:int {
        North = 0,
        East = 1,
        South = 2,
        West = 3 
    }

    /// <summary>
    /// Eje de horientación de la cara del jugador
    /// </summary>
    enum Axis{
        X,
        Y
    }

    /// <summary>
    /// Bloquea la posibilidad de atacar al jugador
    /// </summary>
    bool attackBlocked = false;


    /// <summary>
    /// Partículas de ataque, una para cada dirección
    /// </summary>
    public ParticleSystem[] attackParticles;

    /// <summary>
    /// Hacia donde está mirando el jugador actualmente
    /// </summary>
    Direction playerFaceDirection = Direction.North;

    /// <summary>
    /// Valor del input del eje horizontal
    /// </summary>
    float horizontalAxis = 0f;

    /// <summary>
    /// Valor del input del eje vertical
    /// </summary>
    float verticalAxis = 0f;

    /// <summary>
    /// Velocidad de movmiento del jugador
    /// </summary>
    public float speed = 4f;

    /// <summary>
    /// Ribidbody del jugador
    /// </summary>
    Rigidbody2D playerRb;

    /// <summary>
    /// Animator del jugador
    /// </summary>
    Animator playerAnimator;

    void Start(){
        playerRb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
    }

	// Update is called once per frame
	void Update () {
        detectInput();
    }

    void FixedUpdate() {
        Move();
    }

    /// <summary>
    /// Detecta hacia donde quiere moverse el jugador y si está atacando
    /// </summary>
    void detectInput() {
        horizontalAxis = Input.GetAxisRaw("Horizontal");
        verticalAxis = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(KeyCode.Mouse0) && !attackBlocked){
            attackBlocked = true;
            playerAnimator.SetTrigger("attack");
            StartCoroutine(SlashWithSword());
            
        }
    }

    /// <summary>
    /// Mueve al jugador en la dirección del valor de los ejes
    /// </summary>
    void Move() {
        if(horizontalAxis != 0){
            playerRb.velocity = new Vector2(horizontalAxis * speed, 0);
            assignFaceDirection(Axis.X);
        }
        else {
            playerRb.velocity = new Vector2(0, verticalAxis * speed);
            assignFaceDirection(Axis.Y);
        }

        playerAnimator.SetFloat("speed", Mathf.Abs(playerRb.velocity[0] + playerRb.velocity[1]));
        playerAnimator.SetFloat("movX", playerRb.velocity[0]);
        playerAnimator.SetFloat("movY", playerRb.velocity[1]);
    }


    /// <summary>
    /// Determina hacia donde mira el jugador actualmente
    /// </summary>
    /// <param name="axis"></param>
    void assignFaceDirection(Axis axis){

        if(axis == Axis.X) {
            if(horizontalAxis > 0) {
                playerFaceDirection = Direction.East;
            } 
            if(horizontalAxis < 0) {
                playerFaceDirection = Direction.West;
            }
        } else {
            if (verticalAxis > 0) {
                playerFaceDirection = Direction.North;
            }
            if (verticalAxis < 0) {
                playerFaceDirection = Direction.South;
            }
        }

        playerAnimator.SetFloat("faceDirection", (float)playerFaceDirection);
    }

    /// <summary>
    /// Emite un ataque con las partículas y bloquea la opción de un siguiente hasta que estas terminen
    /// </summary>
    /// <returns>IEnumerator</returns>
    public IEnumerator SlashWithSword() {
        AudioAgent.swordAudio.GetComponent<AudioSource>().Play();
        attackParticles[(int)playerFaceDirection].Emit(30);
        yield return new WaitForSeconds(attackParticles[(int)playerFaceDirection].main.duration);
        attackBlocked = false;
        foreach(Transform child in transform){
            if (child.GetComponent<SlashSwordCollisionController>()) {
                child.GetComponent<SlashSwordCollisionController>().firstCollision = false;
            }
        }
    }


}
