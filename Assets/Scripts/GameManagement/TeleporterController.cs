﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla los puntos de teleportación */

public class TeleporterController : MonoBehaviour {

    /// <summary>
    /// Posición de destino
    /// </summary>
    public Transform destiny;

    /// <summary>
    /// Cuando un jugador entra en un teleporter lo teleporta a la posición de destino
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter2D(Collider2D col) {
        if(col.transform.tag == "Player") {
            col.transform.position = destiny.position;
        }
    }
}
