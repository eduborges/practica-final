﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona los items que están en el suelo del mapa y son recolectables */

public class CollectibleItem : MonoBehaviour {

    /// <summary>
    /// Recurso que otorga el recolectable
    /// </summary>
    public PocketManager.Resources resource;

    /// <summary>
    /// Cantidad del recurso
    /// </summary>
    public int amount = 1;

	// Use this for initialization
	void Start () {
        StartCoroutine(AirFloating());
    }

    /// <summary>
    /// Animación de estar flotando, para llamar la atención del jugador
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator AirFloating() {

        for(int i = 1; i <= 100; i++) {
            if(i < 50)
                transform.position = new Vector3(transform.position.x , transform.position.y + 0.01f);
            if(i > 50)
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.01f);
            yield return new WaitForSeconds(0.02f);
        }
        StartCoroutine(AirFloating());
    }


    /// <summary>
    /// Si el recolectable choca conta el jugador, es obtenido
    /// </summary>
    /// <param name="col">Collider contra el que choca</param>
    void OnTriggerEnter2D(Collider2D col) {
        if(col.transform.tag == "Player"){
            ObtainCollectible();
        }
    }

    /// <summary>
    /// Añade a la cartera del usuario la cantidad de recursos que da el recolectable
    /// </summary>
    public void ObtainCollectible(){
        GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>().AddResources(resource, amount);
        Destroy(gameObject);
    }
}
