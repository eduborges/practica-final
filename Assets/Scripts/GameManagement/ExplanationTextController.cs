﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Controlador de cada texto en créditos y tutorial*/

public class ExplanationTextController : MonoBehaviour {

    /// <summary>
    /// Avisa de que ha terminado su animación y que debe venir el siguiente texto de tutorial
    /// </summary>
    void NextExplanation(){
        GameObject.Find("TutorialManager").GetComponent<TutorialManager>().ReadTutorial();
    }

    /// <summary>
    /// Avisa de que ha terminado su animación y que debe venir el siguiente texto de crédito
    /// </summary>
    void NextCredit() {
        GameObject.Find("CreditManager").GetComponent<CreditsManager>().NextCredit();
    }
}
