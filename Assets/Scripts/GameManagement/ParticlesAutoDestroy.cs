﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que destruye partículas de 1 uso */

public class ParticlesAutoDestroy : MonoBehaviour {

	// Use this for initialization
    // Destruye las partículas instanciadas 3 segundos después de instanciarse
	void Start () {
        Invoke("AutoDestroy", GetComponent<ParticleSystem>().main.duration + 3f);
	}
	
    /// <summary>
    /// Autodestruye el objeto
    /// </summary>
    void AutoDestroy(){
        Destroy(gameObject);
    }
}
