﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

/* Clase que gestiona y centraliza los cambios de escenas */

public class SceneAgent : MonoBehaviour {

    /// <summary>
    /// Carga el menú principal destruyendo la música
    /// </summary>
    public void goToMainMenu() {
        Time.timeScale = 1f;
        Destroy(GameObject.Find("BackgroundSong"));
        SceneManager.LoadScene("MainMenu");
    }

    /// <summary>
    /// Carga la escena del título del juego
    /// </summary>
    public void goToTitleScene() {
        SceneManager.LoadScene("TitleScene");
    }

    /// <summary>
    /// Carga la escena de juego
    /// </summary>
    public void goToNewGame() {
        SceneManager.LoadScene("GameScene");
    }

    /// <summary>
    /// Carga la escena de tutorial
    /// </summary>
    public void goToTutorial(){
        DontDestroyOnLoad(GameObject.Find("BackgroundSong"));
        SceneManager.LoadScene("TutorialScene");
    }

    /// <summary>
    /// Carga la escena de fin de partida
    /// </summary>
    public static void gameOver() {
        SceneManager.LoadScene("GameEnd");
    }

    /// <summary>
    /// Sale del juego
    /// </summary>
    public void exitGame() {
        Application.Quit();
    }

    /// <summary>
    /// Carga la escena de juego
    /// </summary>
    public static void NewGame() {
        SceneManager.LoadScene("GameScene");
    }
}
