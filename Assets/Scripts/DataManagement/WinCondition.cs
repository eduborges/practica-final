﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que almacena los resultados de la partida */

public class WinCondition : MonoBehaviour {

    /// <summary>
    /// Se ha superado el objetivo?
    /// </summary>
    public static bool won = false;

    /// <summary>
    /// Cuantos días se han conseguido pasar?
    /// </summary>
    public static int survDays = 0;

    /// <summary>
    /// Texto de título de la pantalla de fin de juego
    /// </summary>
    public Text title;

    /// <summary>
    /// Texto de subtítulo de la pantalla de fin de juego
    /// </summary>
    public Text subtitle;


    // Use this for initialization
    void Start () {
        if (won) {
            title.color = Color.green;
            title.text = "Lo has conseguido!";
        } else {
            title.color = Color.red;
            title.text = "Has fracasado";
        }

        subtitle.text = subtitle.text + " " + survDays + " días";

	}
	
    /// <summary>
    /// Define si se ha ganado o no y la puntuación obtenida
    /// </summary>
    /// <param name="condition">Ha ganado?</param>
    /// <param name="SurvivedDays">días superados</param>
    public static void HasWin(bool condition, int SurvivedDays) {
        won = condition;
        survDays = SurvivedDays;
    }
}
