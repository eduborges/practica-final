﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona el sistema de cultivo de las zonas fértiles */

public class PlatingMouse : MonoBehaviour {

    /// <summary>
    /// Recurso mínimo y máximo posible a salir con su posibilidad de spawnear
    /// </summary>
    [System.Serializable]
    public struct ResourcesRNG {
        public float probabilityOfSpawn;
        public PocketManager.Resources resources;
        public int minAmount;
        public int maxAmount;
    }

    /// <summary>
    /// Grid que contiene los tilemaps para conocer la celda donde se va a cultivar
    /// </summary>
    GridLayout gridLayout;

    /// <summary>
    /// Agente que modifica el aspecto del terreno tilemap
    /// </summary>
    TilemapAgent tilemapEraser;

    /// <summary>
    /// Última celda que ha sido raycasteada
    /// </summary>
    Vector3Int lastCellHited;

    /// <summary>
    /// Monedero del jugador del que se restarán las semillas y se sumarán los vegetales
    /// </summary>
    PocketManager pocket;

    /// <summary>
    /// Prefab de recolectable a instanciar
    /// </summary>
    public GameObject collectibleItem;

    /// <summary>
    /// Todos los recursos que looteará el plot de cultivo, con sus probabilidades y cantidades de cada uno
    /// </summary>
    public ResourcesRNG[] collectiblesResources;
    

	void Start () {
        gridLayout = GameObject.Find("Grid").GetComponent<GridLayout>();
        tilemapEraser = GameObject.Find("TilemapEraser").GetComponent<TilemapAgent>();
        pocket = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();
    }

    // Hace raycast en la posición del ratón en busca de terreno fertil o recolectables
    // Si es un recolectable lo obtiene
    // Si es terreno fertil intenta plantar una semilla
    void Update() {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Vector2.zero, 0f, LayerMask.GetMask(new string[] {"FertilTerrain", "Collectible"}));

        if (hit) {
            if (hit.transform.tag == "Collectible") {
                hit.transform.GetComponent<CollectibleItem>().ObtainCollectible();
            }
            else {
                lastCellHited = gridLayout.WorldToCell(hit.point);
                TryToPlant(hit.point);
            }
        }

    }

    /// <summary>
    /// Planta una semilla en la última parcela raycasteada si al jugador le quedan semillas y no está ya sembrada
    /// </summary>
    /// <param name="exactWorldPoint">Punto exacto donde se plantó</param>
    void TryToPlant(Vector3 exactWorldPoint) {
        if (Input.GetKeyDown(KeyCode.Mouse1) && !tilemapEraser.isPlanted(lastCellHited) && pocket.Amount(PocketManager.Resources.Seed) > 0) {
            tilemapEraser.plant(lastCellHited);
            pocket.AddResources(PocketManager.Resources.Seed, -1);
            StartCoroutine(IncreasePlant(lastCellHited, exactWorldPoint));
        }
    }

    /// <summary>
    /// Termina el cremiento del cultivo en un plot
    /// </summary>
    /// <param name="plantCell">celda del plot</param>
    /// <param name="exactWorldPoint">punto exacto donde saldrá el loot</param>
    /// <returns>IEnumerator</returns>
    IEnumerator IncreasePlant(Vector3Int plantCell, Vector3 exactWorldPoint) {
        yield return new WaitForSeconds(Random.Range(30, 90));

        tilemapEraser.deplant(plantCell);
        StartCoroutine(CollectiblesRNG(exactWorldPoint));
    }

    /// <summary>
    /// Con las probabilidades decide el loot que se va a spawnear y lo spawnea
    /// </summary>
    /// <param name="exactWorldPoint"></param>
    /// <returns>IEnumerator</returns>
    IEnumerator CollectiblesRNG(Vector3 exactWorldPoint) {
        float coin = Random.Range(1f, 100f);

        foreach(ResourcesRNG collectible in collectiblesResources) {
            if(coin < collectible.probabilityOfSpawn) {
                GameObject go = Instantiate(collectibleItem);
                go.transform.position = exactWorldPoint;
                go.GetComponent<SpriteRenderer>().sprite = pocket.GetResourceImage(collectible.resources);
                go.GetComponent<CollectibleItem>().resource = collectible.resources;
                go.GetComponent<CollectibleItem>().amount = Random.Range(collectible.minAmount, collectible.maxAmount + 1);
            }
            yield return new WaitForSeconds(0.6f);
         
        }
    }
}
