﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla el spawn de los enemigos */

public class EnemySpawner : MonoBehaviour {

    /// <summary>
    /// Tiempo en el que se respawnean todos los enemigos
    /// </summary>
    public float respawnTime = 600f;

    /// <summary>
    /// Tiempo antes del primer respawn
    /// </summary>
    public float firstRespawn = 3f;

    /// <summary>
    /// Enemigo a spawnear
    /// </summary>
    public GameObject enemyPrefab;

	// Use this for initialization
	void Start () {
        InvokeRepeating("SpawnHorde", firstRespawn, respawnTime);
	}

    /// <summary>
    /// Spawnea a todos los enemigos que hayan muerto en sus respectivas posiciones. Las posiciones son hijos de el GameObject de este componente
    /// </summary>
    void SpawnHorde() {
        foreach(Transform child in transform){
            if(child.childCount == 0){
                Instantiate(enemyPrefab, child.transform);
            }
        }
    }
}
