﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/* Clase auxiliar que permite deserializar json en arrays de cualquier tipo de objeto T definido*/
public class JsonHelper{
    public static T[] getJsonArray<T>(string json) {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.array;
    }

    /* Clase que contiene un array de objetos T que se quiera definir */
    [System.Serializable]
    private class Wrapper<T> {
        public T[] array = { };
    }
}
