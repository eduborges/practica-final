﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

/* Clase que gestiona el cambio de tiles en los tilemaps en tiempo de ejecución*/

public class TilemapAgent : MonoBehaviour {

    /// <summary>
    /// Nodo que asocia un nombre con un tilemap
    /// </summary>
    [System.Serializable]
    public struct TilemapNode {
        public string name;
        public Tilemap tilemap;
    }

    /// <summary>
    /// Nodo que asocia un nombre con un tile
    /// </summary>
    [System.Serializable]
    public struct TileNode {
        public string name;
        public TileBase tile;
    }

    /// <summary>
    /// Tilemaps que gestiona
    /// </summary>
    public TilemapNode[] tilemaps;

    /// <summary>
    /// Tiles que gestiona
    /// </summary>
    public TileNode[] tiles;

    /// <summary>
    /// Diccionario que asocia cada tilemap con un nombre
    /// </summary>
    Dictionary<string,Tilemap> dictTilemap = new Dictionary<string, Tilemap>();

    /// <summary>
    /// Diccionario que asocia cada tile con un nombre
    /// </summary>
    Dictionary<string, TileBase> dictTile = new Dictionary<string, TileBase>();

	// Use this for initialization
    // Rellena los diccionarios
	void Start () {

        foreach(TilemapNode node in tilemaps) {
            dictTilemap.Add(node.name, node.tilemap);
        }

        foreach(TileNode node in tiles) {
            dictTile.Add(node.name, node.tile);
        }
    }

    /// <summary>
    /// Abre la puerta visualmente (elimina la puerta con null)
    /// </summary>
    /// <param name="Cell">celda del grid donde está la base de la puerta</param>
    public void openDoor(Vector3Int Cell) {
        dictTilemap["ItemsBehindHeroNotCollide"].SetTile(Cell, null);
        Cell.y++;
        dictTilemap["ItemsBehindHeroCollide"].SetTile(Cell, null);
    }

    /// <summary>
    /// Cierra la puerta visualmente (dibuja una puerta con cada tilebase)
    /// </summary>
    /// <param name="Cell">celda del grid donde está la base de la puerta</param>
    public void closeDoor(Vector3Int Cell) {
        dictTilemap["ItemsBehindHeroNotCollide"].SetTile(Cell, dictTile["ClosedDoor0"]);
        Cell.y++;
        dictTilemap["ItemsBehindHeroCollide"].SetTile(Cell, dictTile["ClosedDoor1"]);
    }

    /// <summary>
    /// Planta en terreno fertil una semilla (efecto de hojitas en la tierra)
    /// </summary>
    /// <param name="Cell">celda donde está el plot de terreno fertil</param>
    public void plant(Vector3Int Cell){
        dictTilemap["FertilTerrain"].SetTile(Cell, dictTile["Plant"]);
    }

    /// <summary>
    /// Desplanta del terreno las semillas (tierra fértil)
    /// </summary>
    /// <param name="Cell"></param>
    public void deplant(Vector3Int Cell) {
        dictTilemap["FertilTerrain"].SetTile(Cell, dictTile["Deplant"]);
    }

    /// <summary>
    /// Comprueba si esa celda está sembrada
    /// </summary>
    /// <param name="Cell">celda a comprobar</param>
    /// <returns>Está sembrada?</returns>
    public bool isPlanted(Vector3Int Cell) {
        if (dictTilemap["FertilTerrain"].GetTile(Cell) == dictTile["Plant"])
            return true;
        else
            return false;
    }
}
