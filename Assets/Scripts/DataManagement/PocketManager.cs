﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que controla el monedero del jugador donde contiene los recursos */

public class PocketManager : MonoBehaviour {

    /// <summary>
    /// Tipo de recurso
    /// </summary>
    public enum Resources {
        Dollar,
        Pirita,
        Icor,
        Vegetable,
        Seed,
        SwordPower
    }

    // Cantidades de cada recurso
    // TODO: usar un diccionario
    public int dollarsAmount;
    public int piritaAmount;
    public int icorAmount;
    public int vegetablesAmount;
    public int seedsAmount;
    public int swordPower;

    // Sprite de cada recurso
    public Sprite dollar;
    public Sprite pirita;
    public Sprite icor;
    public Sprite vegetable;
    public Sprite seed;
    public Sprite sword;

    // Texto de cada recurso que indica la cantidad
    public Text dollarsDisplay;
    public Text piritaDisplay;
    public Text icorDisplay;
    public Text vegetablesDisplay;
    public Text seedsDisplay;
    public Text swordDisplay;

    /// <summary>
    /// Comprueba si la espada está bajo efectos de mejora temporal
    /// </summary>
    public bool improvedSword = false;

    PopupAgent popup;

    // Use this for initialization
    void Start () {
        RefreshPockets();
        popup = GameObject.FindGameObjectWithTag("Popup").GetComponent<PopupAgent>();
    }

    /// <summary>
    /// Refresca los valores de todos los recursos visualmente
    /// </summary>
    void RefreshPockets() {
        dollarsDisplay.text = dollarsAmount.ToString();
        piritaDisplay.text = piritaAmount.ToString();
        icorDisplay.text = icorAmount.ToString();
        vegetablesDisplay.text = vegetablesAmount.ToString();
        seedsDisplay.text = seedsAmount.ToString();
        swordDisplay.text = swordPower.ToString();
    }

    /// <summary>
    /// Añade recursos al monedero
    /// </summary>
    /// <param name="resource">Tipo de recurso a añadir</param>
    /// <param name="amount">Cantidad de recursos</param>
    public void AddResources(Resources resource, int amount) {

        if(amount != 0)
            popup.AmountChanges(amount,resource);

        switch (resource) {
            case Resources.Dollar: { dollarsAmount = dollarsAmount + amount; };break;
            case Resources.Pirita: { piritaAmount = piritaAmount + amount; };break;
            case Resources.Icor: { icorAmount = icorAmount + amount; };break;
            case Resources.Vegetable: { vegetablesAmount = vegetablesAmount + amount; }; break;
            case Resources.Seed: { seedsAmount = seedsAmount + amount; }; break;
            case Resources.SwordPower: { swordPower = swordPower + amount; }; break;
        }

        RefreshPockets();
    }

    /// <summary>
    /// Cantidad actual de un recurso
    /// </summary>
    /// <param name="resource">Tipo de recurso</param>
    /// <returns>Cantidad</returns>
    public int Amount(Resources resource) {
        switch (resource) {
            case Resources.Dollar: { return dollarsAmount; }
            case Resources.Pirita: { return piritaAmount; }
            case Resources.Icor: { return icorAmount; }
            case Resources.Vegetable: { return vegetablesAmount; }
            case Resources.Seed: { return seedsAmount; }
            case Resources.SwordPower: { return swordPower; }
        }
        return 0;
    }

    /// <summary>
    /// Obtiene la imagen de un recurso
    /// </summary>
    /// <param name="resource">Tipo de recurso</param>
    /// <returns>Sprite</returns>
    public Sprite GetResourceImage(Resources resource) {
        switch (resource) {
            case Resources.Dollar: { return dollar; }
            case Resources.Pirita: { return pirita; }
            case Resources.Icor: { return icor; }
            case Resources.Vegetable: { return vegetable; }
            case Resources.Seed: { return seed; }
            case Resources.SwordPower: { return sword; }
        }
        return null;
    }

    /// <summary>
    /// Obtiene el display de texto de un recurso
    /// </summary>
    /// <param name="resource">Tipo de recurso</param>
    /// <returns>Text</returns>
    public Text GetResourceDisplay(Resources resource) {
        switch (resource) {
            case Resources.Dollar: { return dollarsDisplay; }
            case Resources.Pirita: { return piritaDisplay; }
            case Resources.Icor: { return icorDisplay; }
            case Resources.Vegetable: { return vegetablesDisplay; }
            case Resources.Seed: { return seedsDisplay; }
            case Resources.SwordPower: { return swordDisplay; }
        }
        return null;
    }

    /// <summary>
    /// Cambia el color temporalmente del texto de un tipo de recurso
    /// </summary>
    /// <param name="resources">Tipo de recurso</param>
    /// <param name="color">Color</param>
    /// <returns>IEnumerator</returns>
    public IEnumerator ChangeTextColor(Resources resources, Color color) {

        Text display = GetResourceDisplay(resources);
        if (display != null){
            display.color = color;
        }
        
        yield return new WaitForSeconds(0.5f);

        if (display != null){
            if(display == swordDisplay && improvedSword)
                display.color = Color.magenta;
            else
                display.color = Color.black;
        }
    }


}
