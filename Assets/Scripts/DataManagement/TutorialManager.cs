﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona el tutorial del juego */

public class TutorialManager : MonoBehaviour {

    /// <summary>
    /// Representa cada parte del tutorial, distintos conjuntos de explicaciones
    /// </summary>
    [System.Serializable]
    public struct Tutorial {
        public string[] explanations;
    }

    [SerializeField]
    /// <summary>
    /// Índice del tutorial a mostar
    /// </summary>
    int tutorialIndex = 0;

    [SerializeField]
    /// <summary>
    /// Índice del diálogo dentro del tutorial a mostrar
    /// </summary>
    int dialogIndex = 0;

    /// <summary>
    /// Se deben bloquear las acciones del personaje? Usado para mientras sale el texto, no se pueda mover al personaje que no está visible
    /// </summary>
    bool blockActions = true;

    /// <summary>
    /// Posiciones de los escenarios de cada tutorial
    /// </summary>
    public Transform[] tutorialPoints;

    /// <summary>
    /// Diálogos de los tutoriales
    /// </summary>
    public Tutorial[] tutorialDialogs;

    /// <summary>
    /// Texto donde se muestran los diálogos de los tutoriales
    /// </summary>
    public Text tutorialDisplay;

	// Use this for initialization
	void Start () {
        tutorialDisplay.text = tutorialDialogs[0].explanations[0];
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) && !blockActions) {
            blockActions = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().enabled = false;
            if (tutorialIndex < tutorialDialogs.Length) {
                tutorialDisplay.transform.parent.gameObject.SetActive(true);
                tutorialDisplay.text = tutorialDialogs[tutorialIndex].explanations[dialogIndex];
            } else {
                SceneAgent.NewGame();
            }
        }
	}

    /// <summary>
    /// Lee la siguiente explicación del tutorial o si se han terminado las explicaciones y hay que pasar a la acción activa la interacción del jugador
    /// </summary>
    public void ReadTutorial() {
        dialogIndex++;
        if (dialogIndex < tutorialDialogs[tutorialIndex].explanations.Length) {
            tutorialDisplay.text = tutorialDialogs[tutorialIndex].explanations[dialogIndex];
        } else {
            tutorialDisplay.transform.parent.gameObject.SetActive(false);
            GameObject.FindGameObjectWithTag("Player").transform.position = tutorialPoints[tutorialIndex].position;
            tutorialIndex++;
            dialogIndex = 0;
            blockActions = false;
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().enabled = true;
        }
    }
}
