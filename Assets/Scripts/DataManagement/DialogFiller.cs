﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que se encarga de importar la estructura de diálogos de las entidades de juego */

public class DialogFiller {


    /// <summary>
    /// Lee la información del json que coincida con el nombre de la entidad
    /// </summary>
    /// <param name="entityName">Entidad a la que cargar el diálogo</param>
    /// <returns></returns>
    public static EntityController.DialogGraphState Fill(string entityName) {
        int gap = 1;
        TextAsset jsonAsset = Resources.Load<TextAsset>(entityName);
        EntityController.DialogGraphData[] dialogs = JsonHelper.getJsonArray<EntityController.DialogGraphData>(jsonAsset.ToString());
        
        EntityController.DialogGraphState[] dialogState = new EntityController.DialogGraphState[dialogs.Length];
        for (int i = 0; i < dialogs.Length; i++) {
            dialogState[i] = CreateNode(dialogs[i]);
        }
        
        /* 
         La estrucura de los JSON consta de poner cada diálogo como un objeto dentro de un array
         Si isLast es true, el diálogo termina ahí
         Si no lo es, el diálogo que viene a continuación será el siguiente sin asignar
         Por ejemplo: si el primer diálogo da 3 opciones a elegir A B C
         Si se selecciona A el próximo diálogo es el consecutivo en el array
         Si se selecciona B es el siguiente al de A
         Y con C el siguiente al de B 
         Así se va creando un grafo
        */

        for (int i = 0; i < dialogs.Length; i++){
            if (!dialogState[i].data.isLast){
                for (int j = 0; j < dialogState[i].data.content.Length; j++){
                    dialogState[i].next[j] = dialogState[j + gap];
                }
                gap = gap + dialogState[i].data.content.Length;
            }
            
        }

        return dialogState[0];
    }

    /// <summary>
    /// Crea un nodo o estado del grafo de diálogo
    /// </summary>
    /// <param name="data">Contenido leido del JSON</param>
    /// <returns></returns>
    private static EntityController.DialogGraphState CreateNode(EntityController.DialogGraphData data) {
        EntityController.DialogGraphState state = new EntityController.DialogGraphState();
        state.data = data;
        state.next = new EntityController.DialogGraphState[data.content.Length];
        return state;
    }
}
