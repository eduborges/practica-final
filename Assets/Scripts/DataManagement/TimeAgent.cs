﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona el tiempo que transcurre en el juego y todo lo relaccionado con el tiempo en general */

public class TimeAgent : MonoBehaviour {

    /// <summary>
    /// Representa una hora del día
    /// </summary>
    [System.Serializable]
    public struct Daytime {
        public int hours;
        public int minutes;
    }

    /// <summary>
    /// Representa el color del ambiente a una determinada hora
    /// </summary>
    [System.Serializable]
    public struct HourColor {
        public Daytime daytime;
        public Color32 color;
    }

    /// <summary>
    /// Colores del ambiente en cada hora
    /// </summary>
    public HourColor[] hourColors;

    /// <summary>
    /// Objeto que cambia de color según la hora del día
    /// </summary>
    public GameObject enviromentColor;

    /// <summary>
    /// Tiempo de la realidad equivalente a 1 minuto en el juego
    /// </summary>
    public float minuteOnRealTime = 5f;

    /// <summary>
    /// Display donde se visualiza la hora
    /// </summary>
    public GameObject displayDaytime;

    /// <summary>
    /// Hora actual en el juego
    /// </summary>
    public Daytime currentDaytime;

    /// <summary>
    /// Disccionario que asocia cada hora en formato de string con un color de ambiente
    /// </summary>
    Dictionary<string, Color32> dictDaytimeColor = new Dictionary<string, Color32>();

    /// <summary>
    /// Impuestos diarios que cobrará el banco
    /// </summary>
    public int dailyTaxes = 1000;

    /// <summary>
    /// Días que han de pasar para ganar
    /// </summary>
    public int daysToWin = 31;

    /// <summary>
    /// Días que han pasado
    /// </summary>
    int daysCounter = 0;
    // Use this for initialization
    void Start () {
        foreach(HourColor hc in hourColors){
            dictDaytimeColor.Add(DaytimeToKey(hc.daytime), hc.color);
        }
        RefreshTimer();
        InvokeRepeating("MoveAlongTime", minuteOnRealTime, minuteOnRealTime);
    }

    /// <summary>
    /// Avanza el tiempo 1 minuto
    /// </summary>
    void MoveAlongTime() {
        currentDaytime.minutes++;
        if(currentDaytime.minutes == 60) {
            currentDaytime.minutes = 0;
            currentDaytime.hours++;
            if(currentDaytime.hours == 24) {
                currentDaytime.hours = 0;
            }
        }

        RefreshTimer();

        if (dictDaytimeColor.ContainsKey(DaytimeToKey(currentDaytime))){
            changeEnviromentColor(dictDaytimeColor[DaytimeToKey(currentDaytime)]);
        }
    }

    /// <summary>
    /// Refresca el valor del timer en la UI
    /// </summary>
    void RefreshTimer() {
        displayDaytime.GetComponent<Text>().text = DaytimeFormat();
        if(currentDaytime.hours == 0 && currentDaytime.minutes == 0) {
            GetTaxes();
        }
    }

    /// <summary>
    /// Tranforma un daytime en su formato string
    /// </summary>
    /// <param name="dt">Daytime a transformar</param>
    /// <returns>Daytime en formato string</returns>
    string DaytimeToKey(Daytime dt) {
        return dt.hours.ToString() + dt.minutes.ToString();
    }

    /// <summary>
    /// Formatea la hora para mostrarla correctamente
    /// </summary>
    /// <returns>Daytime formateado para mostrar por display</returns>
    string DaytimeFormat(){
        if(currentDaytime.minutes < 10)
            return currentDaytime.hours.ToString() + ":0" + currentDaytime.minutes.ToString();
        else
            return currentDaytime.hours.ToString() + ":" + currentDaytime.minutes.ToString();
    }

    /// <summary>
    /// Cambia el color del ambiente
    /// </summary>
    /// <param name="color">Color del ambiente</param>
    void changeEnviromentColor(Color32 color) {
        enviromentColor.GetComponent<Image>().color = color;
    }

    /// <summary>
    /// Comprueba si la hora actual está entre dos horas (comprobación de horarios de entidades)
    /// </summary>
    /// <param name="open">hora de apertura</param>
    /// <param name="close">hora de cierre</param>
    /// <returns>Se encuentra abierta la entidad?</returns>
    public bool BeetwenDaytimes(Daytime open, Daytime close){

        if(open.hours == currentDaytime.hours){
            if (open.minutes < currentDaytime.minutes)
                return true;
            else
                return false;
        }

        if (close.hours == currentDaytime.hours){
            if (close.minutes > currentDaytime.minutes)
                return true;
            else
                return false;
        }

        if(open.hours > close.hours)
            if(open.hours < currentDaytime.hours || close.hours > currentDaytime.hours)
                return true;

        if(open.hours < close.hours)
            if(open.hours < currentDaytime.hours && currentDaytime.hours < close.hours)
                return true;

        return false;
    }

    /// <summary>
    /// Cambia de día y le paga al banco los impuestos
    /// </summary>
    void GetTaxes() {
        PocketManager pocket = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();

        if(pocket.Amount(PocketManager.Resources.Dollar) < dailyTaxes) {
            WinCondition.HasWin(false, daysCounter);
            SceneAgent.gameOver();
        } else {
            pocket.AddResources(PocketManager.Resources.Dollar, -dailyTaxes);
        }

        daysCounter++;

        if(daysCounter == daysToWin) {
            WinCondition.HasWin(true, daysCounter);
            SceneAgent.gameOver();
        }
    }
}
