﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona y centraliza los audios del juego */

public class AudioAgent : MonoBehaviour {

    public GameObject _dialogIndicatorAudio;
    public static GameObject dialogIndicatorAudio;
    public GameObject _popupGreenSpawnAudio;
    public static GameObject popupGreenSpawnAudio;
    public GameObject _popupRedSpawnAudio;
    public static GameObject popupRedSpawnAudio;
    public GameObject _swordAudio;
    public static GameObject swordAudio;
    public GameObject _enemyDamagedAudio;
    public static GameObject enemyDamagedAudio;

    void Start() {
        dialogIndicatorAudio = _dialogIndicatorAudio;
        popupGreenSpawnAudio = _popupGreenSpawnAudio;
        popupRedSpawnAudio = _popupRedSpawnAudio;
        swordAudio = _swordAudio;
        enemyDamagedAudio = _enemyDamagedAudio;
    }

}
