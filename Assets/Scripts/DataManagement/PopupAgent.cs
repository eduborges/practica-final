﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona los objetos emergentes informativos de perdida u obtención de recursos */

public class PopupAgent : MonoBehaviour {

    /// <summary>
    /// Clase que controla cada popup y su información
    /// </summary>
    public class PopupNode {
        /// <summary>
        /// Cantidad que muestra el popup
        /// </summary>
        public int nodeAmount;
        /// <summary>
        /// Tipo de recurso que muestra el popup
        /// </summary>
        public PocketManager.Resources nodeResource;

        /// <summary>
        /// Constructor del popup
        /// </summary>
        /// <param name="am">cantidad</param>
        /// <param name="re">recurso</param>
        public PopupNode(int am, PocketManager.Resources re){
            nodeAmount = am;
            nodeResource = re;
        }
    }

    /// <summary>
    /// Monedero del jugador
    /// </summary>
    PocketManager pocketManager;

    /// <summary>
    /// Prefab de la estructura básica de un popup a instanciar
    /// </summary>
    public GameObject popupTemplate;

    /// <summary>
    /// Lista de popups a mostrar, para que no se solapen unos con otros
    /// </summary>
    LinkedList<PopupNode> queue = new LinkedList<PopupNode>();

    /// <summary>
    /// Está el agente mostrando acutalmente un popup?
    /// </summary>
    bool busy = false;

    void Start() {
        pocketManager = GameObject.FindGameObjectWithTag("Pocket").GetComponent<PocketManager>();
    }

    /// <summary>
    /// Muestra los cambios producidos en las cantidades de recursos del monedero con un popup, si hay uno mostrándose, se mete en una cola de espera al siguiente para no solaparse
    /// </summary>
    /// <param name="amount">cantidad de cambio producido</param>
    /// <param name="resource">recurso que ha cambiado su cantidad</param>
    public void AmountChanges(int amount, PocketManager.Resources resource) {
        if(queue.Count > 0 || busy){
            queue.AddLast(new PopupNode(amount,resource));
        } else {
            busy = true;
            PopupValue(amount,resource);
        }
    }

    /// <summary>
    /// Instancia el popup como GameObject
    /// </summary>
    /// <param name="amount">cantidad a mostrar</param>
    /// <param name="resource">recurso a mostar</param>
    void PopupValue(int amount, PocketManager.Resources resource) {
        GameObject go = Instantiate(popupTemplate, transform);
        Color popupColor = Color.white;
        Text display = go.GetComponent<Text>();
        if (amount > 0) {
            AudioAgent.popupGreenSpawnAudio.GetComponent<AudioSource>().Play();
            popupColor = Color.green;
        } else if(amount < 0) {
            AudioAgent.popupRedSpawnAudio.GetComponent<AudioSource>().Play();
            popupColor = Color.red;
        } 

        display.color = popupColor;
   
        if (amount > 0)
            display.text = "+" + amount.ToString();
        else
            display.text = amount.ToString();

        if(pocketManager.GetResourceImage(resource) != null)
            go.transform.GetChild(0).GetComponent<Image>().sprite = pocketManager.GetResourceImage(resource);
 
        StartCoroutine(pocketManager.ChangeTextColor(resource, popupColor));
    }

    /// <summary>
    /// Popea al siguiente en la cola si hay
    /// </summary>
    public void NextInQueue() {
        if (queue.Count > 0) {
            PopupNode first = queue.First.Value;
            PopupValue(first.nodeAmount, first.nodeResource);
            queue.RemoveFirst();
        } else {
            busy = false;
        }
    }

    /// <summary>
    /// Popup especial que aparece cuando no existen recursos suficientes para realizar alguna acción
    /// </summary>
    /// <param name="resource">recurso a mostrar</param>
    public void NotEnoughResources(PocketManager.Resources resource) {
        GameObject go = Instantiate(popupTemplate, transform);
        go.GetComponent<Text>().text = "Recursos insuficientes";
        AudioAgent.popupRedSpawnAudio.GetComponent<AudioSource>().Play();
        go.transform.GetChild(0).GetComponent<Image>().sprite = pocketManager.GetResourceImage(resource);
    }
}
