﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Controlador del texto que muestra los diálogos con las entidades */

public class DisplayController : MonoBehaviour {

    /// <summary>
    /// Tipo de diálogo, RawText (texto plano), Interactive (opciones a elegir por parte del usuario), Desconocido
    /// </summary>
    [System.Serializable]
    public enum DisplayInfoType {
        RawText,
        Interactive,
        Unknown
    }

    /// <summary>
    /// Texto que muestra el nombre externo en el cuadro de diálogo
    /// </summary>
    public Text entityDisplay;

    /// <summary>
    /// Texto que muestra el contenido de los diálogos rawtext
    /// </summary>
    public Text textDisplay;

    /// <summary>
    /// Texto que muestra el contenido de los diálogos interactive
    /// </summary>
    public GameObject interactiveDisplay;

    /// <summary>
    /// Entidad actual que está mostrando el diálogo, necesario conocerla para enviar la respuesta de los mensajes interactivos
    /// </summary>
    EntityController actualEntity;

    /// <summary>
    /// Cantidad de opciones máximas que aparecen en un diálogo interactivo
    /// </summary>
    int optionsLenght;

    /// <summary>
    /// Indice de la colocación de la flecha del usuario en un diálogo interactivo
    /// </summary>
    int indicatorIndex = 0;

    /// <summary>
    /// Modo actual en el que trabaja el display para saber que displays tiene que activar y cuales desactivar
    /// </summary>
    public DisplayInfoType currentMode = DisplayInfoType.Unknown;

	void Update () {
        if (currentMode != DisplayInfoType.Unknown)
            ControlUserInput();
    }

    /// <summary>
    /// Muestra un diálogo RawText
    /// </summary>
    /// <param name="entityName">Nombre de la entidad que lo realiza</param>
    /// <param name="text">Texto del mensaje</param>
    /// <param name="entity">Entidad que lo realiza</param>
    public void DisplayRawText(string entityName, string text, EntityController entity) {
        actualEntity = entity;
        PrepareMode(DisplayInfoType.RawText);
        entityDisplay.text = entityName;
        textDisplay.text = text;
    }

    /// <summary>
    /// Muestra un diálogo Interactive
    /// </summary>
    /// <param name="entityName">Nombre de la entidad que lo realiza</param>
    /// <param name="options">Opciones a dar al usuario</param>
    /// <param name="entity">Entidad que lo realiza</param>
    public void DisplayInteractive(string entityName, string[] options, EntityController entity) {
        actualEntity = entity;
        PrepareMode(DisplayInfoType.Interactive, options);
        entityDisplay.text = entityName; 
    }

    /// <summary>
    /// Controlar el Input del usuario según el tipo de diálogo
    /// </summary>
    void ControlUserInput(){
        if (currentMode == DisplayInfoType.Interactive){
            ControlCursor();
        } else if (currentMode == DisplayInfoType.RawText){
            NextDialog();
        }
    }

    /// <summary>
    /// Controla la posición de la flecha que selecciona las opciones en los diálogos interactivos
    /// </summary>
    void ControlCursor() {
        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            AudioAgent.dialogIndicatorAudio.GetComponent<AudioSource>().Play();
            interactiveDisplay.transform.GetChild(1).GetChild(indicatorIndex).gameObject.GetComponent<Image>().enabled = false;
            indicatorIndex = (indicatorIndex + 1) % optionsLenght;
            interactiveDisplay.transform.GetChild(1).GetChild(indicatorIndex).gameObject.GetComponent<Image>().enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            AudioAgent.dialogIndicatorAudio.GetComponent<AudioSource>().Play();
            interactiveDisplay.transform.GetChild(1).GetChild(indicatorIndex).gameObject.GetComponent<Image>().enabled = false;

            if (indicatorIndex > 0)
                indicatorIndex = (indicatorIndex - 1) % optionsLenght;

            interactiveDisplay.transform.GetChild(1).GetChild(indicatorIndex).gameObject.GetComponent<Image>().enabled = true;
        }
        NextDialog();
    }

    /// <summary>
    /// Prepara y configura los GameObject para un tipo de diálogo
    /// </summary>
    /// <param name="mode">Tipo de diálogo que preparar</param>
    /// <param name="opt">Opciones en el caso de que sea interactivo, por defecto es null</param>
    void PrepareMode(DisplayInfoType mode, string[] opt = null) {

        int optlength = 0;

        if (opt != null) {
            optlength = opt.Length;

            if (optlength > 3)
                optlength = 3;

            optionsLenght = optlength;
        }

        if (mode == DisplayInfoType.RawText) {
            textDisplay.gameObject.SetActive(true);
            interactiveDisplay.SetActive(false);
        } else {
            textDisplay.gameObject.SetActive(false);
            interactiveDisplay.SetActive(true);

            for (int i = optlength; i < 3; i++) {
                interactiveDisplay.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
                interactiveDisplay.transform.GetChild(1).GetChild(i).gameObject.SetActive(false);
            }

            for (int i = 0 ; i < optlength; i++) {
                interactiveDisplay.transform.GetChild(0).GetChild(i).gameObject.SetActive(true);
                interactiveDisplay.transform.GetChild(1).GetChild(i).gameObject.SetActive(true);
                interactiveDisplay.transform.GetChild(0).GetChild(i).gameObject.GetComponent<Text>().text = opt[i];
                if(i != indicatorIndex)
                    interactiveDisplay.transform.GetChild(1).GetChild(i).gameObject.GetComponent<Image>().enabled = false;
                else
                    interactiveDisplay.transform.GetChild(1).GetChild(i).gameObject.GetComponent<Image>().enabled = true;
            }
        }
        currentMode = mode;
    }

    /// <summary>
    /// Avanza al siguiente diálogo en el grafo cuando el usuario pulsa espacio y envía el índice de lo que ha seleccionado el usuario, si es RawText siempre enviará 0
    /// </summary>
    void NextDialog() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            int resultIndex = 0;
            currentMode = DisplayInfoType.Unknown;

            if(optionsLenght != 0){
                resultIndex = indicatorIndex % optionsLenght;
            }
            
            indicatorIndex = 0;
            actualEntity.resultReady(resultIndex);  
        }
    }
}
