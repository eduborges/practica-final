﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine;

public class SettingsController : MonoBehaviour {


    /// <summary>
    /// Controlador de audio de la música
    /// </summary>
    public AudioMixer audioMixer;

    /// <summary>
    /// Resoluciones en las que puede estar el juego
    /// </summary>
    Resolution[] resolutions;

    /// <summary>
    /// Dropdown de seleccion de resolución
    /// </summary>
    public Dropdown resolutionDropdown;

    /// <summary>
    /// Panel de opciones
    /// </summary>
    public GameObject settingsPanel;

	// Use this for initialization
    // Establece los valores de las resoluciones en el dropdown y selecciona la actual de la pantalla
	void Start () {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();

        int IndexOfCurrentResolution = 0;
        for(int i = 0; i < resolutions.Length; i++) {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height) {
                IndexOfCurrentResolution = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = IndexOfCurrentResolution;
        resolutionDropdown.RefreshShownValue();
	}
	
    /// <summary>
    /// Cambia el valor del volumen de la música
    /// </summary>
    /// <param name="volume">valor del volumen</param>
    public void SetVolume(float volume){
        audioMixer.SetFloat("volume", volume);
    }
   
    /// <summary>
    /// Cambia el modo de pantalla completa
    /// </summary>
    /// <param name="isFullscreen">Pantalla completa?</param>
    public void SetFullscreen(bool isFullscreen) {
        Screen.fullScreen = isFullscreen;
    }

    /// <summary>
    /// Cambia la resolución de la ventana de juego
    /// </summary>
    /// <param name="index">Índice de la resolución en el dropdown</param>
    public void SetResolution(int index){
        Screen.SetResolution(resolutions[index].width, resolutions[index].height, Screen.fullScreen);
    }

    /// <summary>
    /// Cierra el panel de opciones
    /// </summary>
    public void CloseSettings() {
        settingsPanel.SetActive(false);
    }

    /// <summary>
    /// Abre el panel de opciones
    /// </summary>
    public void OpenSettings() {
        settingsPanel.SetActive(true);
    }
}
