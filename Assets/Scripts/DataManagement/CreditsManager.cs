﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona la suseción de créditos del juego */
public class CreditsManager : MonoBehaviour {

    /// <summary>
    /// Trabajo realizado en el juego y nombre del que lo ha realizado
    /// </summary>
    [System.Serializable]
    public struct Credit {
        public string credit;
        public string name;
    }


    /// <summary>
    /// Índice del crédito actual a mostrar
    /// </summary>
    int creditIndex = 0;

    /// <summary>
    /// Todos los créditos que deben mostrarse
    /// </summary>
    public Credit[] credits;

    /// <summary>
    /// Texto que aparece como título del crédito
    /// </summary>
    public Text creditTitle;

    /// <summary>
    /// Texto con el nombre de al que se le atribuye el crédito
    /// </summary>
    public Text creditName;

    /// <summary>
    /// Panel de créditos que tapa el fondo ya que están en una misma escena que el menú
    /// </summary>
    public GameObject creditPanel;


    /// <summary>
    /// Inicia los créditos
    /// </summary>
    public void StartCredits() {
        creditPanel.SetActive(true);
        creditTitle.text = credits[0].credit;
        creditName.text = credits[0].name;
    }

    /// <summary>
    /// Cambia los textos al del siguiente crédito
    /// </summary>
    public void NextCredit() {
        creditIndex++;
        if (creditIndex < credits.Length) {
            creditTitle.text = credits[creditIndex].credit;
            creditName.text = credits[creditIndex].name;
        }
        else {
            EndCredits();
        }
    }

    /// <summary>
    /// Termina los créditos y vuelve a mostrar el menú
    /// </summary>
    void EndCredits() {
        creditPanel.SetActive(false);
        creditIndex = 0;
    }
}
